import { combineReducers } from 'redux';
import institutesReducer from './reducers/institutes';
import instituteReducer from './reducers/institute';
import instituteEnReducer from './reducers/instituteEn';
import instituteNameReducer from './reducers/instituteName';
import branchesReducer from './reducers/branches';
import licensesReducer from './reducers/licenses';
import pillarsReducer from './reducers/pillars';

import citiesReducer from './reducers/cities';
import provincesReducer from './reducers/provinces';
import instituteTypesReducer from './reducers/instituteTypes';
import instituteKindsReducer from './reducers/instituteKinds';
import activityTypesReducer from './reducers/activityTypes';
import licenseTypesReducer from './reducers/licenseTypes';

import changeLanguageReducer from './reducers/changeLanguage';

export default combineReducers({
    institutes: institutesReducer,
    institute: instituteReducer,
    instituteEn: instituteEnReducer,
    instituteNames: instituteNameReducer,
    branches: branchesReducer,
    licenses: licensesReducer,
    pillars: pillarsReducer,
    cities: citiesReducer,
    provinces: provincesReducer,
    instituteTypes: instituteTypesReducer,
    instituteKinds: instituteKindsReducer,
    activityTypes: activityTypesReducer,
    licenseTypes: licenseTypesReducer,
    currentLanguage: changeLanguageReducer
})
