export default function (state = {
    instituteTypes: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTETYPES_FULFILLED':
            return { ...state, fetched: true, instituteTypes: action.payload };
        case 'FETCH_INSTITUTETYPES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}