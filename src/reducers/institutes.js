export default function (state = {
    institutes: [],
    total: 0,
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTES_EMPTY':
            return { ...state, fetched: true, institutes: [], total: 0};
        case 'FETCH_INSTITUTES_FULFILLED':
            return { ...state, fetched: true, institutes: action.payload.data, total: action.payload.total};
        case 'FETCH_INSTITUTES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}