export default function (state = {
    pillars: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_PILLARS_FULFILLED':
            return { ...state, fetched: true, pillars: action.payload };
        case 'FETCH_PILLARS_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
    
}