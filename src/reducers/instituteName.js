export default function (state = {
    names: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTENAME_FULFILLED':
            return { ...state, fetched: true, names: action.payload };
        case 'FETCH_INSTITUTENAME_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }

}