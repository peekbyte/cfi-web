export default function (state = {
    instituteEn: {},
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTEEN_FULFILLED':
            return { ...state, fetched: true, instituteEn: action.payload };
        case 'FETCH_INSTITUTEEN_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }

}