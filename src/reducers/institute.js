export default function (state = {
    institute: {},
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTE_FULFILLED':
            return { ...state, fetched: true, institute: action.payload };
        case 'FETCH_INSTITUTE_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}