export default function (state = {
    licenseTypes: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_LICENSETYPES_FULFILLED':
            return { ...state, fetched: true, licenseTypes: action.payload };
        case 'FETCH_LICENSETYPES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;

    }
}