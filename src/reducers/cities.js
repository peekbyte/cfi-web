export default function (state = {
    cities: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_CITIES_FULFILLED':
            return { ...state, fetched: true, cities: action.payload };
        case 'FETCH_CITIES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
    
}