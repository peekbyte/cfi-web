export default function (state = {
    lang: 'fa'
}, action) {
    switch (action.type) {
        case 'CHANGE_LANGUAGE_SUCCESS':
            return { ...state, lang: action.payload };
        default:
            return state;
    }
}