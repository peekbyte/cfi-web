export default function (state = {
    activityTypes: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_ACTIVITYTYPES_FULFILLED':
            return { ...state, fetched: true, activityTypes: action.payload };
        case 'FETCH_ACTIVITYTYPES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}