export default function (state = {
    licenses: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_LICENSES_FULFILLED':
            return { ...state, fetched: true, licenses: action.payload };
        case 'FETCH_LICENSES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}