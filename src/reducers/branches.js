export default function (state = {
    branches: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_BRANCHES_FULFILLED':
            return { ...state, fetched: true, branches: action.payload };
        case 'FETCH_BRANCHES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
    
}