export default function (state = {
    instituteKinds: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_INSTITUTEKINDS_FULFILLED':
            return { ...state, fetched: true, instituteKinds: action.payload };
        case 'FETCH_INSTITUTEKINDS_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
    
}