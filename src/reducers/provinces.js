export default function (state = {
    provinces: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_PROVINCES_FULFILLED':
            return { ...state, fetched: true, provinces: action.payload };
        case 'FETCH_PROVINCES_REJECTED':
            return { ...state, fetched: false, error: true };
        default:
            return state;
    }
}