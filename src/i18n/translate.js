/* eslint react/prefer-stateless-function: "off" */
/* Because stateless functions don't have context it seems */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import en from './en';
import fa from './fa';

const languages = { en, fa };

export default function translate(key) {
    return Component => {
        let lang = 'fa';
        if (window.location.pathname.split('/')[1])
            lang = window.location.pathname.split('/')[1];

        function stateToProps(state) {
            return {
                currentLanguage: lang
            }
        }

        // const stateToProps = state => ({

        //     currentLanguage: lang
        // });

        class TranslationComponent extends React.Component {
            render() {
                const strings = languages[this.props.currentLanguage][key];
                const merged = {
                    ...this.props.strings,
                    ...strings
                };
                if (strings) {
                    return (
                        <Component {...this.props}
                            strings={merged}
                            currentLanguage={this.props.currentLanguage}
                        />
                    );
                }

                return (
                    <Component {...this.props}
                        currentLanguage={this.props.currentLanguage}
                    />
                );
            }
        }

        TranslationComponent.propTypes = {
            strings: PropTypes.object,
            currentLanguage: PropTypes.string
        };

        return connect(stateToProps)(TranslationComponent);
    };
}