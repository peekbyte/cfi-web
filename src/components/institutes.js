import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchInstitutes, fetchInstitutesEmpty } from '../actions/institutes';
import { fetchInstituteName } from '../actions/institutes';
import Autosuggest from 'react-autosuggest';
import translate from '../i18n/translate';

import { convertToP, commaSeprator, replaceYeKe } from '../libs/helpers';

import { fetchActivityTypes, fetchCities, fetchInstituteTypes, fetchLicenseTypes, fetchProvinces, fetchInstituteKinds } from '../actions/baseData';

import { Link } from 'react-router-dom';

import topScroll from '../images/topScroll.png';
import removeIcon from '../images/remove-icon.png';
import searchIcon from '../images/search-icon.png';
import loadingDash from '../images/loading-dash.gif';
import websiteIcon from '../images/website.png';

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.Name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
    <div>
        {suggestion.Name}
    </div>
);

class Institutes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            instituteType: '',
            instituteKind: '',
            activityType: '',
            licenseType: '',
            city: '',
            province: '',
            name: '',
            limit: 10,
            offset: 1,
            isLoading: false,
            showTopScroll: false,
            suggestionValue: '',
            suggestions: []
        };

        let me = this;

        const queryString = require('query-string');
        const params = queryString.parse(props.location.search);
        this.state = Object.assign(this.state, params)

        let lang = this.props.currentLanguage;

        this.props.fetchInstitutes(this.state, lang);
        this.props.fetchCities(null, lang);
        this.props.fetchProvinces(lang);
        this.props.fetchInstituteTypes(lang);
        this.props.fetchLicenseTypes(null,lang);
        this.props.fetchInstituteKinds(null, lang);

        if (this.state.instituteType != '') {
            this.props.fetchActivityTypes(this.state.instituteType, lang);
            this.props.fetchInstituteKinds(this.state.instituteType, lang);
        }

        if (this.state.province != '') {
            this.props.fetchCities(this.state.province, lang);
        }

        window.onscroll = function () {
            me.setState({ showTopScroll: window.scrollY > 200 })

            if (window.scrollY >= (document.body.offsetHeight - window.innerHeight - 120) && !me.props.institutes.fetching && me.state.isLoading.toString() == "false" &&
                me.props.institutes.institutes.length < me.props.institutes.total) {
                me.setState({ isLoading: true });
                setTimeout(() => { me.addPage() }, 500);
            }
        }
    }

    componentDidMount() {
        let me = this;
    }

    componentWillReceiveProps(next) {
    }

    handleChange(event) {
        let propName = event.target.name;

        if (propName === 'province') {
            this.props.fetchCities(event.target.value, this.props.currentLanguage);
        }
        if (propName === 'instituteType') {
            this.setState({ instituteKind: '', activityType: '' },
                this.props.fetchActivityTypes(event.target.value, this.props.currentLanguage),
                this.props.fetchInstituteKinds(event.target.value, this.props.currentLanguage)
            );
        }

        this.setState({ [propName]: event.target.value, limit: 10, offset: 1 }, this.doSearch);
    }

    doSearch() {
        const queryString = require('query-string');
        let q = {};
        for (let item in this.state) {
            if (this.state[item] !== '' && item != 'isLoading' && item != 'showTopScroll' && item != 'suggestionValue') {
                q[item] = this.state[item]
            }
        }
        this.props.history.push('/' + this.props.currentLanguage + '/?' + queryString.stringify(q))
        this.props.fetchInstitutes(this.state, this.props.currentLanguage).then(() => {
            this.setState({ isLoading: false })
        });
    }

    addPage() {
        this.setState({ offset: parseInt(this.state.offset) + 1, isLoading: true }, this.doSearch);
    }

    goToDetail(id) {
        this.props.history.push('/' + this.props.currentLanguage + '/institute/' + id)
    }

    clearFilters() {
        this.setState({
            instituteType: '',
            instituteKind: '',
            activityType: '',
            licenseType: '',
            city: '',
            province: '',
            name: '',
            suggestionValue: '',
            limit: 10,
            offset: 1
        }, this.doSearch,
            this.props.fetchActivityTypes(null, this.props.currentLanguage),
            this.props.fetchInstituteKinds(null, this.props.currentLanguage),
            this.props.fetchCities(this.state.province, this.props.currentLanguage));
           
    }

    // logChange(val) {
    //     this.setState({ name: val == null ? '' : val.value }, this.doSearch, );
    // }

    // getOptions = function (input, callback) {
    //     this.props.fetchInstituteName(input).then(data => {
    //         callback(null, {
    //             options: data.map((x) => { return { value: x.Name, label: x.Name } }),
    //             complete: false
    //         });
    //     });
    // };


    onChange = (event, { newValue }) => {
        this.setState({
            suggestionValue: newValue
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        value = replaceYeKe(value);
        this.props.fetchInstituteName(value, this.props.currentLanguage).then(data => {
            this.setState({
                suggestions: data
            });
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (value) => {
        let searchvalue = ""
        if(value.target.value == "0" || value.target.value == undefined)
            searchvalue = value.target.firstChild.textContent
        else
            searchvalue = value.target.value
        this.setState({
            name: searchvalue
        }, this.doSearch);

    };

    render() {

        const { suggestionValue, suggestions } = this.state;
        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: this.props.strings.instituteNamePlaceHolder,
            value: suggestionValue,
            onChange: this.onChange
        };

        return (
            <div>
                {
                    this.state.showTopScroll ?
                        <div className="top-scroll" onClick={() => { window.scrollTo(0, 0); }}><img src={topScroll} /></div> :
                        null
                }
                <section className="filters">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>{this.props.strings.instituteName}</label>
                                    <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        getSuggestionValue={getSuggestionValue}
                                        renderSuggestion={renderSuggestion}
                                        onSuggestionSelected={this.onSuggestionSelected}
                                        inputProps={inputProps}
                                    />

                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>{this.props.strings.instituteType}</label>
                                    <select className="form-control" name="instituteType" value={this.state.instituteType} onChange={this.handleChange.bind(this)}>
                                        <option></option>
                                        {this.props.instituteTypes.instituteTypes.map(item => {
                                            return <option value={item.ID} key={item.ID}>{item.Name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>

                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>{this.props.strings.instituteKind}</label>
                                    <select className="form-control" name="instituteKind" value={this.state.instituteKind} onChange={this.handleChange.bind(this)}>
                                        <option></option>
                                        {this.props.instituteKinds.instituteKinds.map(item => {
                                            return <option value={item.ID} key={item.ID}>{item.Name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>{this.props.strings.activityType}</label>
                                    <select className="form-control" name="activityType" value={this.state.activityType} onChange={this.handleChange.bind(this)}>
                                        <option></option>
                                        {this.props.activityTypes.activityTypes.map(item => {
                                            return <option value={item.ID} key={item.ID}>{item.Name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row"> <div className="col-xs-12 col-sm-6 col-md-3">
                            <div className="form-group">
                                <label>{this.props.strings.licenseType}</label>

                                <select className="form-control" name="licenseType" value={this.state.licenseType} onChange={this.handleChange.bind(this)}>
                                    <option></option>
                                    {this.props.licenseTypes.licenseTypes.map(item => {
                                        return <option value={item.OldId} key={item.OldId} >{item.OldName}</option>
                                    })}
                                </select>
                            </div>
                        </div>
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>{this.props.strings.province}</label>
                                    <select className="form-control" name="province" value={this.state.province} onChange={this.handleChange.bind(this)}>
                                        <option></option>
                                        {this.props.provinces.provinces.map(item => {

                                            return <option value={item.ID} key={item.ID}>{item.Name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            {/*<div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>شهـــر</label>
                                    <select className="form-control">
                                        <option></option>
                                        {this.props.cities.cities.map(item => {
                                            return <option value={item.Id} key={item.Id}>{item.City1}</option>
                                        })}
                                    </select>

                            </div>*/}
                            {/*<div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>حوزه فعـــالیت</label>
                                    <select className="form-control">
                                        <option>آپشن 1</option>
                                        <option>آپشن 2</option>
                                    </select>
                                </div>
                            </div>*/}
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <div className="form-group">
                                    <label>&nbsp; </label>
                                    <a className="remove-filters" onClick={this.clearFilters.bind(this)}>
                                        <img src={removeIcon} />
                                        {this.props.strings.restFilters}
                                    </a>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-3">
                                <label>&nbsp; </label>
                                <div className="form-group record-found">   {convertToP(commaSeprator(this.props.institutes.total), this.props.currentLanguage)} {this.props.strings.recordFound}</div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="grid-style">
                    <div className="container">
                        <div className="row">
                            <div className=" col-xs-12">
                                <table className="rwd-table main-table ">
                                    <thead>
                                        <tr>
                                            <th >{this.props.strings.row} </th>
                                            <th >{this.props.strings.instituteName}  </th>
                                            <th >{this.props.strings.instituteType}</th>
                                            <th >{this.props.strings.instituteKind} </th>
                                            <th >{this.props.strings.ceo}/{this.props.strings.ceoSandogh} </th>
                                            <th >{this.props.strings.seoRegisterNo}  </th>
                                            {/*<th >شماره تماس </th>*/}
                                            {/* <th >تاریخ پایان فعالیت </th> */}
                                            <th >{this.props.strings.website}  </th>
                                            <th> &nbsp; </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.institutes.institutes.map((item, i) => {
                                            return (<tr key={i}>
                                                <td className='text-center' data-th={this.props.strings.row + ":"} onClick={() => { this.goToDetail(item.Id) }} > {convertToP(i + 1, this.props.currentLanguage)}</td>
                                                <td data-th={this.props.strings.instituteName + ":"} onClick={() => { this.goToDetail(item.Id) }} > {item.Name ? item.Name : '\u00A0'}</td>
                                                <td data-th={this.props.strings.instituteType + ":"} onClick={() => { this.goToDetail(item.Id) }} > {item.InstituteType ? item.InstituteType : '\u00A0'} </td>
                                                <td data-th={this.props.strings.instituteKind + ":"} onClick={() => { this.goToDetail(item.Id) }}> {item.InstituteKind ? item.InstituteKind : '\u00A0'} </td>
                                                <td data-th={this.props.strings.ceo + '/' + this.props.strings.ceoSandogh + ":"} onClick={() => { this.goToDetail(item.Id) }}> {item.CEO ? item.CEO : '\u00A0'}</td>
                                                <td className='text-center' data-th={this.props.strings.seoRegisterNo + ":"} onClick={() => { this.goToDetail(item.Id) }}>{item.SEORegisterNo ? convertToP(item.SEORegisterNo, this.props.currentLanguage) : '\u00A0'} </td>
                                                {/*<td> {item.Phone? convertToP(item.Phone) :'\u00A0' } </td>*/}
                                                {/* <td> 1396/00/00 </td> */}
                                                <td className="text-center" data-th={this.props.strings.website + ":"}  > {item.Website ? <a target="_blank" href={'http://' + item.Website}><img src={websiteIcon} /></a> : '\u00A0'} </td>
                                                <td>
                                                    <Link className="detail-button text-center" to={'/' + this.props.currentLanguage + '/institute/' + item.Id}>
                                                        <img src={searchIcon} />{this.props.strings.details}
                                                    </Link>
                                                </td>
                                            </tr>)
                                        })
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div id="page-selection">
                                {this.state.isLoading.toString() == 'true' ? <div className="grid-loading"><span>{this.props.strings.loading}</span>
                                    <img src={loadingDash} />
                                </div> : null}
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        institutes: state.institutes,
        cities: state.cities,
        provinces: state.provinces,
        activityTypes: state.activityTypes,
        instituteTypes: state.instituteTypes,
        instituteKinds: state.instituteKinds,
        licenseTypes: state.licenseTypes
    }
}

export default connect(mapStateToProps, {
    fetchInstitutes, fetchInstitutesEmpty, fetchInstituteName, fetchActivityTypes,
    fetchCities, fetchInstituteTypes, fetchInstituteKinds, fetchLicenseTypes, fetchProvinces
})(translate('Institutes')(Institutes));