import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import translate from '../i18n/translate'


import { fetchInstitute, fetchInstituteEn, fetchBranches, fetchLicenses, fetchPillars } from '../actions/institutes';
import { convertToP, dateSeprator, commaSeprator } from '../libs/helpers';
import profileIcon from '../images/profile-icon.png';
import topScroll from '../images/topScroll.png';
import sazmanLogo from '../images/sazman-logo.png';
import sabtLogo from '../images/sabt-logo.png';
import certificatesLogo from '../images/certificates.png';
import branchesLogo from '../images/branches.png';
import pillarsLogo from "../images/pillars-icon.png";
import classNames from 'classnames/bind';

class Institute extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showTopScroll: false
        };

        const instituteId = this.props.match.params.id;
        this.props.fetchInstitute(instituteId, this.props.currentLanguage);
        this.props.fetchBranches(instituteId, this.props.currentLanguage);
        this.props.fetchLicenses(instituteId, this.props.currentLanguage);
        this.props.fetchPillars(instituteId, this.props.currentLanguage);

        let me = this;
        window.onscroll = function () {
            me.setState({ showTopScroll: window.scrollY > 200 })
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    isSandogh(instituteType) {
        if (instituteType === 6 ||
            instituteType === 8 ||
            instituteType === 15 ||
            instituteType === 17 ||
            instituteType === 24)
            return true;
        else
            return false;
    }

    hasNotLicense(instituteType) {
        // if (instituteType === 6 ||
        //     instituteType === 8 ||
        //     instituteType === 10 ||
        //     instituteType === 11 ||
        //     instituteType === 12 ||
        //     instituteType === 15 ||
        //     instituteType === 17 ||
        //     instituteType === 21 ||
        //     instituteType === 24)
        if (instituteType === 1 || instituteType === 3 || instituteType === 2 || instituteType === 12 || instituteType === 9)
            return false;
        else
            return true;

    }

    isBroker(instituteType) {
        if (instituteType === 9)
            return true;
        else
            return false;
    }

    getCeoTitle(instituteType) {
        if (this.isSandogh(instituteType))
            return this.props.strings.ceoSandogh + ":";
        else
            return this.props.strings.ceo + ":";
    }

    render() {
        return (
            <div>
                {
                    this.state.showTopScroll ?
                        <div className="top-scroll" onClick={() => { window.scrollTo(0, 0); }}><img src={topScroll} /></div> :
                        null
                }
                <section className="profile">
                    <div className="container">
                        <div className="icon">
                            <span className="hr-inner"></span>
                            <img src={profileIcon} />
                            <span className="hr-inner"></span>
                        </div>
                        <div className="title">
                            <h2> {this.props.strings.subtitle} </h2>

                        </div>
                        <div className="row form-content">
                            <div className="farsi col-xs-12 ">
                                <div className="row name-row">
                                    <div className="col-xs-12">
                                        <img src={sazmanLogo} />
                                        <span className="name">{this.props.strings.bourseHeader}</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.instituteName}</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.instituteType}:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.InstituteType} {this.props.institute.InstituteKind ?  "(" + this.props.institute.InstituteKind + ")" : null }</label>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.seoRegisterNo}:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{convertToP(this.props.institute.SEORegisterNo, this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.seoRegisterDate}:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{convertToP(dateSeprator(this.props.institute.SEORegisterDate), this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-5 control-label">{this.props.strings.listedCapital}:</label>
                                            <label className="col-xs-12 col-sm-7 control-label">{convertToP(commaSeprator(this.props.institute.ListedCapital), this.props.currentLanguage)}</label>
                                        </div>
                                    </div>

                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">سرمایه ثبت نشده:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">ISIC:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.ISIC}</label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">ISIN:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.ISIN}</label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">وضعیت:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">پایان سال مالی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}
                                    {/*{this.isBroker(this.props.institute.InstituteTypeId) ?
                                        null :
                                        (<div className="col-xs-12 col-md-6">
                                            <div className="form-group">
                                                <label className="col-xs-12 col-sm-4 control-label">موضوع فعالیت:</label>
                                                <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.ActivitySubject}</label>
                                            </div>
                                        </div>)}*/}
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label"> {this.getCeoTitle(this.props.institute.InstituteTypeId)}
                                            </label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.CEO}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.website}:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"><a href={'http://' + this.props.institute.Website} target="_blank"> {this.props.institute.Website}</a> </label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.email}:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Email}</label>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-md-6 address-fix-height" >
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.officeAddress}:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{convertToP(this.props.institute.Address, this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">{this.props.strings.officePhone}:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{convertToP(this.props.institute.Phone, this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">نمابر دفتر مرکزی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}

                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">مدیر مالی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">حسابرس و بازرس قانونی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">بازرس علی البدل:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}


                                </div>


                                <div className="row name-row">
                                    <div className="col-xs-12">
                                        <img src={sabtLogo} />
                                        <span className="name">{this.props.strings.sabtHeader}</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.nationalId + ":"}</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{convertToP(this.props.institute.NationalId, this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.registerPlace + ":"}</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{this.props.institute.RegisterPlace}</label>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.registerNo + ":"}</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{convertToP(this.props.institute.RegisterNo, this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-4  control-label">{this.props.strings.registerDate + ":"}</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{convertToP(dateSeprator(this.props.institute.RegisterDate), this.props.currentLanguage)}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="english col-xs-12 col-md-6">
                                <div className="row name-row">
                                    <div className="col-xs-12 col-md-6">
                                        <img src={sazmanLogo} />
                                        <span className="name">Securities And Exchange Organization</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12 col-md-6 ">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4  control-label">Name:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.instituteEn.Name}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4  control-label">Type:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>

                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">SEO Register Number:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{this.props.institute.SEORegisterNo}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">SEO Register Date:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{dateSeprator(this.props.institute.SEORegisterDateEn)}</label>

                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Listed Capital:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{commaSeprator(this.props.institute.ListedCapital)}</label>

                                        </div>
                                    </div>*/}

                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">سرمایه ثبت نشده:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.ISIC}</label>
                                            <label className="col-xs-12 col-sm-4 control-label">ISIC:</label>
                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">

                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.ISIN}</label>
                                            <label className="col-xs-12 col-sm-4 control-label">ISIN:</label>
                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                            <label className="col-xs-12 col-sm-4 control-label">State:</label>

                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">پایان سال مالی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                        </div>
                                    </div>*/}

                            {/*  {this.isBroker(this.props.institute.InstituteTypeId) ?
                                        null :
                                        (<div className="col-xs-12 col-md-6">
                                            <div className="form-group">
                                                <label className="col-xs-12 col-sm-4 control-label">Activity Subject:</label>
                                                <label className="col-xs-12 col-sm-8  control-label">{this.props.instituteEn.ActivitySubject}</label>

                                            </div>
                                        </div>)}


                                    <div className="col-xs-12 col-md-6 address-fix-height">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Office Address:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.instituteEn.Address}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Office Phone:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.instituteEn.Phone}</label>


                                        </div>
                                    </div>
                                    {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-8  control-label"></label>
                                            <label className="col-xs-12 col-sm-4 control-label">نمابر دفتر مرکزی:</label>

                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">CEO:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.instituteEn.CEO}</label>

                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">مدیر مالی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">حسابرس و بازرس قانونی:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}
                            {/*<div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">بازرس علی البدل:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Name}</label>
                                        </div>
                                    </div>*/}
                            {/* <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Website:</label>
                                            <label className="col-xs-12 col-sm-8  control-label"><a href={'http://' + this.props.institute.Website} target="_blank"> {this.props.institute.Website}</a> </label>


                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Email:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.Email}</label>


                                        </div>
                                    </div>

                                </div>
                                <div className="row name-row">
                                    <div className="col-xs-12 col-md-6">
                                        <img src={sabtLogo} />
                                        <span className="name">Deeds and Real Estate Registration Organization</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4  control-label">National Id:</label>
                                            <label className="col-xs-12 col-sm-8  control-label">{this.props.institute.NationalId}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Register Place:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{this.props.institute.RegisterProvinceEn}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Register Date:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{dateSeprator(this.props.institute.RegisterDateEn)}</label>

                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group">
                                            <label className="col-xs-12 col-sm-4 control-label">Register Number:</label>
                                            <label className="col-xs-12 col-sm-8 control-label">{this.props.institute.RegisterNo}</label>

                                        </div>
                                    </div>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </section>

                {this.hasNotLicense(this.props.institute.InstituteTypeId) ? null
                    : (<section className="certificates">
                        <div className="container">
                            <div className="icon">
                                <span className="hr-inner"></span>
                                <img src={certificatesLogo} />
                                <span className="hr-inner"></span>
                            </div>
                            <div className="title">
                                <h2>{this.props.strings.certificateHeader}</h2>
                                {/*<h2>Certificates</h2>*/}
                            </div>
                            <div className="row form-content">


                                <table className="rwd-table ">
                                    <thead >
                                        <tr>
                                            <th >{this.props.strings.row}</th>
                                            <th >{this.props.strings.licenseType} </th>
                                            <th >{this.props.strings.licenseNo} </th>
                                            {/*<th>تـاریخ صـدور</th>*/}
                                            <th >{this.props.strings.startDate} </th>
                                            <th >{this.props.strings.expireDate} </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        {this.props.licenses.map((item, i) => {
                                            return <tr key={item.Id}>
                                          {console.log(item.InstituteId)}
                                                <td className='text-center' data-th={this.props.strings.row + ":"}> {convertToP(i + 1, this.props.currentLanguage)}</td>
                                                <td data-th={this.props.strings.licenseType + ":"}>{item.LicenseType ? convertToP(item.LicenseType, this.props.currentLanguage) : '\u00A0'}</td>
                                                <td  data-th={this.props.strings.licenseNo + ":"} className={classNames({ 'red-row': item.LicenseNo === "تعليق از خريد" , "text-center" : 1===1 })}> { item.LicenseNo === "تعليق از خريد" ? item.InstituteId === 54 ? <span className="tooltiptext">با توجه به نامه کانون کارگزاران و مصوبه هیئت مدیره سازمان بورس و اوراق بهادار فعالیت شرکت کارگزاری مبنی بر خرید اوراق بهادار در بورس تهران و فرابورس ایران تا اطلاع ثانوی تعلیق گردید.</span> : <span className="tooltiptext">با توجه به مصوبه هیات مدیره سازمان بورس و اوراق بهادار، کارگزاری شاخص سهام از تاریخ 1396/10/09 به مدت یکسال از دریافت یا اجرای هر گونه سفارش خرید یا اتخاذ موقعیت تعهدی جدید، محروم گردید.</span> :""} {item.LicenseNo ? convertToP(item.LicenseNo, this.props.currentLanguage) : '\u00A0'}</td>

                                                {/*<td>{item.IssueDate ? convertToP(dateSeprator(item.IssueDate)) : '\u00A0'}</td>*/}
                                                <td className='text-center' data-th={this.props.strings.startDate + ":"}>{item.StartDate ? convertToP(dateSeprator(item.StartDate), this.props.currentLanguage) : '\u00A0'}</td>
                                                <td className='text-center' data-th={this.props.strings.expireDate + ":"}>{item.ExpireDate ? convertToP(dateSeprator(item.ExpireDate), this.props.currentLanguage) : '\u00A0'}</td>
                                            </tr>
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>)
                }

                <section className="branches">
                    <div className="container">
                        <div className="icon">
                            <span className="hr-inner"></span>
                            <img src={branchesLogo} />
                            <span className="hr-inner"></span>
                        </div>
                        <div className="title">
                            <h2>{this.props.strings.branchHeader}</h2>
                            {/*<h2>Branches</h2>*/}
                        </div>
                        <div className="row form-content">


                            <table className="rwd-table ">
                                <thead>
                                    <tr>
                                        <th > {this.props.strings.row}</th>
                                        <th > {this.props.strings.activityType}</th>
                                        <th > {this.props.strings.branchType}</th>
                                        {/*<th>نــام  </th>*/}
                                        <th > {this.props.strings.province}</th>
                                        <th > {this.props.strings.city}</th>
                                        <th > {this.props.strings.address}</th>
                                        <th > {this.props.strings.phone}</th>
                                        <th > {this.props.strings.branchCEO}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.branches.map((item, i) => {
                                        return <tr key={item.Id}>
                                            <td className='text-center' data-th={this.props.strings.row + ":"}> {convertToP(i + 1, this.props.currentLanguage)}</td>
                                            <td data-th={this.props.strings.activityType + ":"}>{item.ActivityType ? convertToP(item.ActivityType, this.props.currentLanguage) : '\u00A0'}</td>
                                            <td data-th={this.props.strings.branchType + ":"}>{item.BranchType ? convertToP(item.BranchType, this.props.currentLanguage) : '\u00A0'}</td>
                                            {/*<td>{item.Name ? item.Name : '\u00A0'}</td>*/}
                                            <td data-th={this.props.strings.province + ":"}>{item.Province ? item.Province : '\u00A0'}</td>
                                            <td data-th={this.props.strings.city + ":"}>{item.City ? item.City : '\u00A0'}</td>
                                            <td data-th={this.props.strings.address + ":"}>{item.Address ? convertToP(item.Address, this.props.currentLanguage) : '\u00A0'}</td>
                                            <td className='text-center' data-th={this.props.strings.phone + ":"}>{item.Phone ? convertToP(item.Phone, this.props.currentLanguage) : '\u00A0'}</td>
                                            <td data-th={this.props.strings.branchCEO + ":"}>{item.CEO ? item.CEO : '\u00A0'}</td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

                {
                    this.isSandogh(this.props.institute.InstituteTypeId) ? (
                        <section className="pillars">
                            <div className="container">
                                <div className="icon">
                                    <span className="hr-inner"></span>
                                    <img src={pillarsLogo} />

                                    <span className="hr-inner"></span>
                                </div>
                                <div className="title">
                                    <h2> {this.props.strings.pillarHeader}</h2>
                                </div>
                                <div className="row form-content">
                                    <table className="rwd-table ">
                                        <thead>
                                            <tr>
                                                <th>{this.props.strings.row}  </th>
                                                <th>{this.props.strings.pillarName} </th>
                                                <th>{this.props.strings.pillarType}  </th>
                                                <th>{this.props.strings.personType}</th>
                                                <th> {this.props.strings.pillarNationalCode}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.props.pillars.map((item, i) => {
                                                return <tr key={item.Id}>
                                                    <td className='text-center' data-th={this.props.strings.row}> {convertToP(i + 1, this.props.currentLanguage)}</td>
                                                    <td data-th={this.props.strings.pillarName}>{item.Name ? convertToP(item.Name, ) : '\u00A0'}</td>
                                                    <td data-th={this.props.strings.pillarType}>{item.PillarType ? convertToP(item.PillarType, this.props.currentLanguage) : '\u00A0'}</td>
                                                    <td data-th={this.props.strings.personType}>{item.PersonType ? convertToP(item.PersonType, this.props.currentLanguage) : '\u00A0'}</td>
                                                    <td className='text-center' data-th={this.props.strings.pillarNationalCode} >{item.PillarNationalCode ? convertToP(item.PillarNationalCode, this.props.currentLanguage) : '\u00A0'}</td>

                                                </tr>
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    ) : null
                }


            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        institute: state.institute.institute,
        instituteEn: state.instituteEn.instituteEn,
        branches: state.branches.branches,
        licenses: state.licenses.licenses,
        pillars: state.pillars.pillars

    }
}


Institute.propTypes = {
    strings: PropTypes.object
};

Institute.defaultProps = {
    strings: {
        someTranslatedText: 'Hello World',
        subtitle: 'AAAA',
    }
};

export default connect(mapStateToProps, { fetchInstitute, fetchBranches, fetchLicenses, fetchPillars })(translate('Institute')(Institute));