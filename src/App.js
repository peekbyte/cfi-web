import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { connect } from "react-redux";
import store from './store';
import Institutes from './components/institutes.js';
import Institute from './components/institute.js';
import ContactUs from './components/contactUs.js';
import { Link, BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import translate from './i18n/translate';

import logdingLogo from './images/loading-logo.png';
import loadingGif from './images/loading.gif';
import logo from './images/logo.png';
import footerLogo from './images/footer-logo.png';

import topScroll from './images/topScroll.png';
import user from './images/user.png';

import logdingLogoLtr from './images/loading-logo-ltr.png';
import logoLtr from './images/logo-ltr.png';
import footerLogoLtr from './images/footer-logo-ltr.png';
import { changeLanguage } from './actions/changeLanguage';

import {convertToP} from './libs/helpers';

class App extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      showTopScroll: false
    };

    let me = this;
    window.onscroll = function () {
      me.setState({ showTopScroll: window.scrollY > 200 })
    }
  }

  componentWillMount() {
    if (this.props.currentLanguage === 'fa') {
        // require('./bootstrap.rtl.min.css');
        // require('./index-ltr.css');
    } else {
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 1);
  }

  refreshNewLang() {
    window.location.href = '/' + (this.props.currentLanguage === 'fa' ? 'en' : 'fa');
  }

  render() {
    const { loading } = this.state;

    if (loading) {
      return <div className="bg-loading">
        <div className="full-width">
          <img src={this.props.currentLanguage === 'fa' ? logdingLogo : logdingLogoLtr} className="loading-logo" />
        </div>
        <div className="full-width">
          <img src={loadingGif} className="loading-gif" />
        </div>
      </div>;
    }
    else
      return (

        <Router>
          <div className={this.props.currentLanguage == 'en' ? "ltr" : ''}>
            {
              this.state.showTopScroll ?
                <div className="top-scroll" onClick={() => { window.scrollTo(0, 0); }}><img src={topScroll} /></div> :
                null
            }
            <header>
              <div className="container header">
                <Link to={'/'}> <img src={this.props.currentLanguage == 'fa' ? logo : logoLtr} /> </Link>
              </div>
            </header>
            <nav>
              <div className="container">
                <div className="menu">
                  <ul className="main-menu">
                    <li className="selected">
                      <Link to={'/'  +  this.props.currentLanguage}>
                        {this.props.strings.homeLink}
                      </Link>
                    </li>
                    {/*<li>
                        <Link to={'/contactus'}>
                          ارتباط با ما
                                      </Link>
                      </li>*/}
                  </ul>
                </div>
                <div className="small-toolbar">
                  {/*<a className="login-button" rel="noopener noreferrer" target="_blank" href="http://manage.cfi.codal.ir">
                      <img className="user" src={user} />
                      ورود کــــاربـران
                      {/*</a>*/}
                  {/*<Link className="lang-button" rel="noopener noreferrer" to={this.props.currentLanguage === 'fa' ? '/en' : '/fa'}>

                  </Link>*/}

                  {/*<button value="changeLang" onClick={()=> {this.props.changeLanguage(this.props.currentLanguage === 'fa'? 'en': 'fa')}} >changeLang</button>*/}

                  {/* <button className="lang-button" value="changeLang" onClick={this.refreshNewLang.bind(this)} >{this.props.currentLanguage === 'fa' ? 'EN' : 'فا'}</button> */}
                </div>
              </div>
            </nav>
            <div>
              <Route exact path="/" render={() => (<Redirect to="/fa" />)} />
              <Route exact path="/:lang/" component={Institutes} />
              <Route exact path="/:lang/institute/:id" component={Institute} />
              <Route exact path="/:lang/contactus" component={ContactUs} />
            </div>
            <div className="footer-nav">
              <div className="container">
                <div className="menu">
                  <ul className="small-menu">
                    <li>
                      <Link to={'/'}>
                        {this.props.strings.homeLink}
                      </Link>

                    </li>
                    {/*<li>  <Link to={'/contactus'}>
                        ارتباط با ما
                                      </Link></li>*/}
                  </ul>
                </div>
              </div>
            </div>
            <footer>
              <div className="container">
                <img src={this.props.currentLanguage == 'fa' ? footerLogo : footerLogoLtr} />
                <div className="footer-text">
                  <p>© {this.props.strings.copyRight}</p>
                  <p> <span>{this.props.strings.developedBy} </span> <a target="_blank" href="http://rayanbourse.ir/">{this.props.strings.company}</a></p>
                  <p>{this.props.strings.devVersion} <span >{this.props.currentLanguage == 'fa' ? convertToP('1396/10/12',this.props.currentLanguage): '1396/10/12'}</span></p>
                </div>
              </div>
              
            </footer>
          </div>
        </Router>

      );
  }
}

export default connect(null, { changeLanguage })(translate('App')(App));
