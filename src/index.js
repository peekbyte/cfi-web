import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';


import App from './App';
import './custom.css';
import './custom-ltr.css';


import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Provider store={store}>
        <App></App>
    </Provider >
    , document.getElementById('root'));
// registerServiceWorker();