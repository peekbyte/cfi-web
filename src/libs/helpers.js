export function convertToP(input, lang) {
    if (!input)
        return input;
    if(lang !== 'fa')
        return input;
        
    let result = input.toString().replace(new RegExp('1', 'g'), '۱');
    result = result.replace(new RegExp('2', 'g'), '۲');
    result = result.replace(new RegExp('3', 'g'), '۳');
    result = result.replace(new RegExp('4', 'g'), '۴');
    result = result.replace(new RegExp('5', 'g'), '۵');
    result = result.replace(new RegExp('6', 'g'), '۶');
    result = result.replace(new RegExp('7', 'g'), '۷');
    result = result.replace(new RegExp('8', 'g'), '۸');
    result = result.replace(new RegExp('9', 'g'), '۹');
    result = result.replace(new RegExp('0', 'g'), '۰');
    return result;

}

export function replaceYeKe(input) {
    if (!input)
        return input;
    let result = input.toString().replace(new RegExp('ك', 'g'), 'ک');
    result = result.replace(new RegExp('ي', 'g'), 'ی');
    return result;

}

export function commaSeprator(input){
    if (!input)
        return input;
    return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
export function dateSeprator(input) {
    if (!input)
        return input;

    let result = input.toString();

    if (result.length < 8)
        return result;


    return result.substring(0, 4) + '/' + result.substring(4, 6) + '/' + result.substring(6, 8)
}