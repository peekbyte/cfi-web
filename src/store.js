import { applyMiddleware, createStore } from 'redux';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import reducers from './reducers';

const middleware = applyMiddleware(promise(), thunk)

const store = createStore(reducers, middleware)

store.subscribe(() => {
})

export default store;
