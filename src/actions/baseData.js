import axios from 'axios';

export function fetchProvinces(lang) {
    return function (dispatch) {
        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'provinces?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_PROVINCES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_PROVINCES_REJECTED', payload: error })
        })
    }
}

export function fetchCities(province, lang) {
    return function (dispatch) {
        const q = 'lng=' + lang + '&province=' + (province ? province : '');
        const config = { headers: { 'Accept-Language': lang } }

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'cities?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_CITIES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_CITIES_REJECTED', payload: error })
        })
    }
}

export function fetchInstituteTypes(lang) {
    return function (dispatch) {
        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'instituteTypes?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_INSTITUTETYPES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_INSTITUTETYPES_REJECTED', payload: error })
        })
    }
}

export function fetchInstituteKinds(instituteType, lang) {
    
    const q = 'lng=' + lang + '&instituteType=' + (instituteType ? instituteType : '');
    const config = { headers: { 'Accept-Language': lang } }
    return function (dispatch) {
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'instituteKinds?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_INSTITUTEKINDS_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_INSTITUTEKINDS_REJECTED', payload: error })
        })
    }
}

export function fetchActivityTypes(instituteTypeId, lang) {

    return function (dispatch) {
        if (!instituteTypeId)
            instituteTypeId = '';

        if (instituteTypeId.toString() !== '9')
            dispatch({ type: 'FETCH_ACTIVITYTYPES_FULFILLED', payload: [] })
        else {
            const config = { headers: { 'Accept-Language': lang } }
            const q = 'lng=' + lang
            axios.get(process.env.REACT_APP_ENDPOINT_URL + 'activityTypes?' + q, config).then((response) => {
                dispatch({ type: 'FETCH_ACTIVITYTYPES_FULFILLED', payload: response.data })
            }).catch((error) => {
                dispatch({ type: 'FETCH_ACTIVITYTYPES_REJECTED', payload: error })
            })
        }
    }
}

export function fetchLicenseTypes(instituteTypeId,lang) {
    if (!instituteTypeId)
    instituteTypeId = '0';

    return function (dispatch) {
        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/'+instituteTypeId+'/licenseTypes?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_LICENSETYPES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_LICENSETYPES_REJECTED', payload: error })
        })
    }
}