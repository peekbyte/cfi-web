export function changeLanguage(lang) {
    return function (dispatch) {
        dispatch({ type: 'CHANGE_LANGUAGE_SUCCESS', payload: lang })
    }
}
