import axios from 'axios';

export function fetchInstitutesEmpty() {
    return function (dispatch) {
        dispatch({ type: 'FETCH_INSTITUTES_EMPTY'})
    }
}
export function fetchInstitutes(search = {}, lang ) {
    
    return function (dispatch) {
        return new Promise((resolve, reject) => {

            // dispatch({ type: 'FETCH_INSTITUTES_EMPTY'})

            // const headers = { headers: { 'Accept-Language': lang } }
            const q = 'lng=' + lang
                + '&name=' + search.name 
                + '&name=' + search.name
                + '&city=' + (search.city ? search.city : '')
                + '&province=' + (search.province ? search.province : '')
                + '&instituteType=' + (search.instituteType ? search.instituteType : '')
                + '&instituteKind=' + (search.instituteKind ? search.instituteKind : '')
                + '&activityType=' + (search.activityType ? search.activityType : '')
                + '&licenseType=' + (search.licenseType ? search.licenseType : '')
                + '&limit=' + search.limit + '&offset=' + search.offset;
            axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes?' + q).then((response) => {

                dispatch({ type: 'FETCH_INSTITUTES_FULFILLED', payload: response.data })
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'FETCH_INSTITUTES_REJECTED', payload: error })
                reject(error);
            })
        });
    }
}

export function fetchInstitute(id, lang ) {
    return function (dispatch) {
        const q = 'lng=' + lang;
        const config = { headers: { 'Accept-Language': lang } }
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/' + id + '?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_INSTITUTE_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_INSTITUTE_REJECTED', payload: error })
        })
    }
}

export function fetchInstituteEn(id, lang ) {
    return function (dispatch) {

        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institute/en/' + id + '?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_INSTITUTEEN_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_INSTITUTEEN_REJECTED', payload: error })
        })
    }
}

export function fetchBranches(instituteId, lang ) {
    return function (dispatch) {
        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/' + instituteId + '/branches' + '?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_BRANCHES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_BRANCHES_REJECTED', payload: error })
        })
    }
}

export function fetchLicenses(instituteId, lang ) {
    return function (dispatch) {

        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/' + instituteId + '/licenses' + '?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_LICENSES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_LICENSES_REJECTED', payload: error })
        })
    }
}

export function fetchPillars(instituteId, lang ) {
    return function (dispatch) {

        const config = { headers: { 'Accept-Language': lang } }
        const q = 'lng=' + lang

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/' + instituteId + '/pillars' + '?' + q, config).then((response) => {
            dispatch({ type: 'FETCH_PILLARS_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_PILLARS_REJECTED', payload: error })
        })
    }
}

export function fetchInstituteName(name, lang ) {
    return function (dispatch) {
        return new Promise((resolve, reject) => {

            const config = { headers: { 'Accept-Language': lang } }
            const q = 'lng=' + lang;
            //+ '&name=' + name;
            if(name === null){name = '';}
            return axios.get(process.env.REACT_APP_ENDPOINT_URL + 'institutes/'+ name +'?' + q, config).then((response) => {
                dispatch({ type: 'FETCH_INSTITUTENAME_FULFILLED', payload: response.data })
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'FETCH_INSTITUTENAME_REJECTED', payload: error })
                reject(error);
            })

        });
    }
}